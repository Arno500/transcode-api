# Summary
I couldn't find any self-hosted API for a conversion service so I made my own. It aim to support latest video and audio codecs, for best quality and efficiency.

In the future it'll probably support generating DASH/HLS manifest and have a web interface for users.
# Environment variables
Variable | Description | Default  
--- | --- | ---
`API_PASS` | (NOT IMPLEMENTED YET) A token used to query the API and get files (to prevent public use) | `123`
`USE_PATH_FFMPEG` | Use FFMPEG from path instead of included one | _`false`_
`CONVERSION_THREADS` | The number of parrallel conversion processes | `1`
`THREADS_RATIO` | The ratio of used threads/available threads | `0.5`
`REDIS_HOST` | The IP or hostname of a Redis instance | `127.0.0.1`
`REDIS_PORT` | The port were the Redis instance can be joined | `6379`
`REDIS_PASS` | The password used to login to this instance | _`null`_
`REDIS_DB` | The DB where the queues will be stored (note that multi-db is discouraged in Redis) | _`null`_

> By default, this tool will use only **half** of the processing power of your machine. This is made on purpose to leave a bit of space for other processes. The main use for this application is in containerised environments allowing priority fine-tuning without using request limits in Kubernetes or Docker (they're not really efficient and unpredictable for the underlying process).

If you want to increase CPU usage and enable faster encodings, either increase the number of parrallel processes or the ratio of used cores per process. You should not exceed the capacity of your system (ie: `THREADS_RATIO * CONVERSION_THREADS` should not be more than 1 and probably less than 0.9) as this will actually reduce the efficiency of the encoding process and overload globally your machine (probably crashing some softwares).

# Encoding recommendations

## VP9

## H264

## AV1
Do not use it now, wait a year.

# Special notes
This software uses a special FFMPEG build for Windows, including SVT-AV1 encoder.
When encoding to AV1 (why would you do that, after all?), we use SVT-AV1 for better performances. This allow us to run conversion at arround 2-4 FPS instead of 0.3 for the AOMedia one, with a little downgrade in quality. As this encoder is still in an early state, it's not included by default in FFMPEG and even conflicts with the AOMedia's. Thus, we need to redistribute a special build for every platforms (note that I can't build for macOS for obvious reasons).
Also, SVT-AV1 requires a lot of memory to work (see here: https://github.com/OpenVisualCloud/SVT-AV1#hardware)
I only included it in Windows as this is a test for now. You really should use VP9 on the web, currently.  