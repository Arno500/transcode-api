import { downloadFFMPEGBinaries } from "./utils/platform";
import { startServer } from "./libs/server";
(async function main() {
    downloadFFMPEGBinaries();
    startServer();
})();

// MVP
// TODO: Compression settings
// TODO: Poster generation
// TODO: Autoclean

// Later
// TODO: Compressed file upload
// TODO: Video profiles (+ DASH/HLS)
