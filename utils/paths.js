import path from "path";
import url from "url";
import fs from "fs";

/**
 * Return the absolute path to the root of the program
 * @returns {string} The absolute path to the root of the program
 */
export const getRootPath = () => {
    return path.resolve(url.fileURLToPath(import.meta.url), "../..");
};

const rootPath = getRootPath();

/**
 * Returns the bin path subfolder, and create it if it doesn't exists
 * @returns {string} The absolute path to the .bin subfolder of your platform (that contains the FFMPEG executables for your platform)
 */
export const getBinPath = (platform) => {
    const binPath = path.resolve(rootPath, "data/.bin", platform);
    if (!fs.existsSync(binPath)) fs.mkdirSync(binPath, { recursive: true });
    return binPath;
};
/**
 * Returns the temp path, and create it if it doesn't exists
 * @returns {string} The absolute path to the temp folder (that contains upload parts)
 */
export const getTempPath = () => {
    const tempPath = path.resolve(rootPath, "data/.tmp");
    if (!fs.existsSync(tempPath)) fs.mkdirSync(tempPath, { recursive: true });
    return tempPath;
};
/**
 * Returns the temp upload path, and optionnaly create it
 * @param {string} hash The hash of the file to find the folder from
 * @param {boolean} create Create the folder if it doesn't exists
 * @returns {string} The absolute path to the upload folder of the file (that contains upload parts)
 */
export const getTempFilePath = (hash, create) => {
    const tempUploadPath = path.resolve(getTempPath(), hash);
    if (!fs.existsSync(tempUploadPath)) {
        if (create) {
            fs.mkdirSync(tempUploadPath, { recursive: true });
            return tempUploadPath;
        } else {
            return false;
        }
    } else {
        return tempUploadPath;
    }
};
/**
 * Returns the path to the status file
 * @param {string} hash The hash of the file to find the status from
 * @param {boolean} create Create the underlying folder if it doesn't exists
 * @returns {string} The absolute path to the status file of the file uploading
 */
export const getTempStatusPath = (hash, create) => {
    const filePath = getTempFilePath(hash, create);
    if (filePath) return path.resolve(filePath, "status");
    return false;
};
/**
 * Returns the path to the output file
 * @param {string} hash The hash of the file to get the output file from
 * @param {boolean} create Create the underlying folder if it doesn't exists
 * @returns {string} The absolute path to the output file resulting of the chunked uploads
 */
export const getTempOutputPath = (hash, create) => {
    const filePath = getTempFilePath(hash, create);
    if (filePath) return path.resolve(filePath, "output");
    return false;
};
/**
 * Returns the path to a given chunk
 * @param {string} hash The hash of the file to get the output file from
 * @param {string} chunk A chunk (basically a filename)
 * @returns {string} The absolute path to the chunk
 */
export const getTempChunkPath = (hash, chunk) => {
    const filePath = getTempFilePath(hash);
    if (filePath) return path.resolve(filePath, String(chunk));
    return false;
};
/**
 * Returns the transcoded files path, and create it if it doesn't exists
 * @returns {string} The absolute path to the transcoded files folder (that contains finished transcoded files)
 */
export const getTranscodedPath = () => {
    const transcodedPath = path.resolve(rootPath, "data/transcoded");
    if (!fs.existsSync(transcodedPath))
        fs.mkdirSync(transcodedPath, {
            recursive: true,
        });
    return transcodedPath;
};
/**
 * Returns the transcoded files path, and create it if needed
 * @param {string} hash The hash of the file to to get the path from
 * @param {boolean} create Create the folder if it doesn't exists
 * @returns {string} The absolute path to the transcoded files folder (that contains finished transcoded files)
 */
export const getTranscodedFilePath = (hash, create) => {
    const transcodedPath = path.resolve(getTranscodedPath(), hash);
    if (!fs.existsSync(transcodedPath)) {
        if (create) {
            fs.mkdirSync(transcodedPath, {
                recursive: true,
            });
            return transcodedPath;
        } else {
            return false;
        }
    } else {
        return transcodedPath;
    }
};
/**
 * Returns the path to the transcoded file
 * @param {string} hash The hash of the file to find the status from
 * @param {string} filename The filename to find
 * @param {boolean} create Create the folder if it doesn't exists
 * @returns {string} The absolute path to the transcoded file
 */
export const getTranscodedOutputPath = (hash, filename, create) => {
    const filePath = getTranscodedFilePath(hash, create);
    if (filePath) return path.resolve(filePath, filename);
    return false;
};
/**
 * Returns the path to the status file of transcoded file
 * @param {string} hash The hash of the file to find the status from
 * @param {boolean} create Create the folder if it doesn't exists
 * @returns {string} The absolute path to the status file of the file transcoded
 */
export const getTranscodedStatusPath = (hash, create) => {
    const filePath = getTranscodedFilePath(hash, create);
    if (filePath) return path.resolve(filePath, "status");
    return false;
};
/**
 * Returns the video processor file path
 * @returns {string} The absolute path to the .js video processor file
 */
export const getVideoProcessorPath = () => {
    return path.resolve(rootPath, "libs/video.js");
};
/**
 * @param {Array} filesArray
 * @param {string} folder
 * @returns {boolean} True if every file already exists in the folder. False otherwise.
 */
export const filesExistsInFolder = (filesArray, folder) => {
    const doesExists = filesArray.map((file) =>
        fs.existsSync(path.resolve(folder, file))
    );
    return doesExists.every((elm) => elm === true);
};
