import path from "path";
import fs from "fs";

import got from "got";
import tar from "tar-stream";
import lzma from "lzma-native";
import unzipper from "unzipper";
// import ffmpeg from "fluent-ffmpeg";

import { setFfmpegPath, setFfprobePath } from "../libs/ffmpeg";
import { filesExistsInFolder, getBinPath } from "./paths";

export const currentPlatform = process.platform;

/**
 * @returns {Array} An array containing the ffmpeg binary in the first entry, and ffplay in the second
 */
export const getFFMPEGBinaries = () => {
    let returnValue = [];
    if (currentPlatform === "win32") {
        returnValue = ["ffmpeg.exe", "ffprobe.exe"];
    } else {
        returnValue = ["ffmpeg", "ffprobe"];
    }
    return returnValue;
};

export const downloadAndExtract = (url) => {
    return new Promise((resolve, reject) => {
        const ffmpegFilenames = getFFMPEGBinaries();
        const binPath = getBinPath();
        const stream = got.stream(url);
        const extract = (stream, filePath) => {
            console.log("Extracting " + filePath);
            stream.pipe(fs.createWriteStream(filePath));
        };
        if (path.extname(url) === ".xz") {
            stream
                .pipe(lzma.createDecompressor())
                .pipe(tar.extract())
                .on("entry", function extractTarFiles(headers, stream, next) {
                    const fileName = path.basename(headers.name);
                    stream.on("end", next);
                    if (ffmpegFilenames.some((elm) => elm.match(fileName))) {
                        extract(stream, path.resolve(binPath, fileName));
                    } else {
                        stream.resume();
                    }
                })
                .on("finish", resolve);
        } else if (path.extname(url) === ".zip") {
            stream
                .pipe(unzipper.Parse())
                .on("entry", function extractZipFiles(entry) {
                    const fileName = path.basename(entry.path);
                    if (ffmpegFilenames.some((elm) => elm.match(fileName))) {
                        extract(entry, path.resolve(binPath, fileName));
                    } else {
                        entry.autodrain();
                    }
                })
                .on("close", resolve);
        }
    });
};

export const downloadFFMPEGBinaries = async () => {
    let urls = [];
    if (currentPlatform === "win32") {
        // urls = [
        // "https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-latest-win64-static.zip",
        // ];
    } else if (currentPlatform === "darwin") {
        urls = [
            "https://evermeet.cx/ffmpeg/ffmpeg-4.2.2.zip",
            "https://evermeet.cx/ffmpeg/ffprobe-4.2.2.zip",
        ];
        console.warn(
            "I can't build anything for macOS (thanks, Apple), thus I can't create a custom FFMPEG build including AV1. The downloaded build here only contains H264 and VP9 encoders."
        );
    } else if (currentPlatform === "linux") {
        urls = [
            "https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz",
        ];
    } else {
        console.error(
            "Your platform is not currently supported. Please add FFMPEG binaries and download links for your platform in the utils/platform.js here: https://gitlab.com/Arno500/transcode-api. Currently using ffmpeg and ffprobe from your system. Please be aware that they may not be up-to-date and may not support latest codecs and formats."
        );
    }
    const binPath = getBinPath(currentPlatform);
    const ffmpegFilenames = getFFMPEGBinaries();
    if (!process.env.USE_PATH_FFMPEG) {
        if (urls.length >= 1) {
            if (!filesExistsInFolder(ffmpegFilenames, binPath)) {
                console.log("Some FFMPEG files are missing, downloading...");
                for (const url of urls) {
                    await downloadAndExtract(url);
                }
            }
        }
        setFfmpegPath(path.resolve(binPath, ffmpegFilenames[0]));
        setFfprobePath(path.resolve(binPath, ffmpegFilenames[1]));
        console.log("FFMPEG is now ready");
    }
};
