import { Queue, Worker } from "bullmq";
import { getVideoProcessorPath } from "../utils/paths";
import videoProc from "./video";

const CONVERSION_THREADS = process.env.CONVERSION_THREADS || 1;

const REDIS_HOST = process.env.REDIS_HOST || "127.0.0.1";
const REDIS_PORT = process.env.REDIS_PORT || 6379;
const REDIS_PASS = process.env.REDIS_PASS;
const REDIS_DB = process.env.REDIS_DB;

const queueOpts = {
    redis: {
        port: REDIS_PORT,
        host: REDIS_HOST,
        password: REDIS_PASS,
        db: REDIS_DB,
    },
};

export const videoQueue = new Queue("videoTranscoding", queueOpts);
export const uploadCleaningQueue = new Queue("uploadCleaning", queueOpts);
//getVideoProcessorPath()
const videoProcessor = new Worker("videoTranscoding", videoProc, {
    redis: { ...queueOpts.redis },
    concurrency: CONVERSION_THREADS,
});

const uploadCleaningProcessor = new Worker(
    "uploadCleaning",
    (job) => {
        console.log("Cleaned old files");
    },
    queueOpts
);
