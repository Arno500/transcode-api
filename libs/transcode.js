import fs from "fs";

import md5File from "md5-file";

import {
    getTranscodedPath,
    getTempPath,
    getTempStatusPath,
    getTranscodedStatusPath,
    getTempOutputPath,
    getTempChunkPath,
    getTempFilePath,
} from "../utils/paths";
import { uploadCleaningQueue } from "./queues";

export const createNewTranscode = (opts) => {
    return new Promise(async (resolve, reject) => {
        const initialObject = await initializeTranscodeStatus(opts);
        await uploadCleaningQueue.add(
            opts.filename,
            { hash: opts.hash },
            { jobId: opts.hash, delay: 1000 * 60 * 60 * 24 * 3 }
        );
        resolve(initialObject);
    });
};

export const initializeTranscodeStatus = (opts) => {
    return new Promise(async (resolve, reject) => {
        const initialObject = {
            status: "uploading",
            position: 0,
            filename: opts.filename,
            size: opts.size,
            settings: opts.settings,
            chunksStatus: Array(opts.chunks).fill(false),
        };
        const result = await writeTranscodeStatus(opts.hash, initialObject);
        resolve(result);
        // fs.writeFile(
        //     getTempStatusPath(opts.hash, true),
        //     JSON.stringify(initialObject),
        //     (err) => {
        //         if (err) console.error(err);
        //         resolve(initialObject);
        //     }
        // );
    });
};
/**
 *
 *
 * @param {*} hash The hash of the file to get the status from
 * @param {*} raw Return result as JS data or as a string directly without parsing (true)
 * @returns {string|object} Status data as string (if `raw` is true) or as JS object
 */
export const retrieveTranscodeStatus = (hash, raw = false) => {
    return new Promise((resolve, reject) => {
        let path;
        if (fs.existsSync(getTranscodedStatusPath(hash))) {
            path = getTranscodedStatusPath(hash);
        } else {
            path = getTempStatusPath(hash);
        }
        if (!path)
            reject({
                code: 404,
                message: "Could not find file path for hash: " + hash,
            });

        fs.readFile(path, (err, data) => {
            if (err) {
                console.error(err);
                resolve(null);
            }
            resolve(raw ? data : JSON.parse(data));
        });
    });
};
/**
 *
 * @param {string} hash The hash of the file to get the status from
 * @param {object} status Status object to store in the status file
 */
export const writeTranscodeStatus = (hash, status) => {
    return new Promise((resolve, reject) => {
        let path;
        if (fs.existsSync(getTranscodedStatusPath(hash))) {
            path = getTranscodedStatusPath(hash);
        } else {
            path = getTempStatusPath(hash, true);
        }
        if (!path)
            reject({
                code: 404,
                message: "Could not find file path for hash: " + hash,
            });
        const string = JSON.stringify(status);
        fs.writeFile(path, string, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            resolve(string);
        });
    });
};

export const assembleChunks = async (hash, chunkCount) => {
    return new Promise(async (resolve, reject) => {
        const outputPath = getTempOutputPath(hash);
        if (fs.existsSync(outputPath)) fs.unlinkSync(outputPath);
        const outputStream = fs
            .createWriteStream(outputPath, {
                flags: "a",
                autoClose: false,
            })
            .on("error", console.error);
        console.log("Joining file " + hash);
        for (let index = 0; index < chunkCount; index++) {
            await (() =>
                new Promise((resolve) => {
                    const chunkStream = fs.createReadStream(
                        getTempChunkPath(hash, index)
                    );
                    chunkStream.on("close", () => {
                        chunkStream.close();
                        resolve();
                    });
                    chunkStream
                        .pipe(outputStream, { end: false })
                        .on("error", console.error);
                }))();
        }
        try {
            outputStream.end();
            outputStream.close();
            const md5 = await md5File(outputPath);
            if (md5.toUpperCase() !== hash.toUpperCase()) reject();
            // for (let index = 0; index < chunkCount; index++) {
            //     fs.unlinkSync(getTempChunkPath(hash, index));
            // }
            resolve();
        } catch (err) {
            console.error(err);
        }
    });
};

export const copyStatus = (hash) => {
    const tempStatusPath = getTempStatusPath(hash);
    const outputStatusPath = getTranscodedStatusPath(hash, true);
    fs.copyFileSync(tempStatusPath, outputStatusPath);
};

/**
 * @returns {Array} A list of folders in the converted folder (corresponding to original file's hash)
 */
export const listTranscodeFolders = () => {
    const convertedPath = getTranscodedPath();
    return fs.readdirSync(convertedPath);
};
/**
 * @returns {Array} A list of folders in the upload folder (corresponding to original file's hash)
 */
export const listUploadFolders = () => {
    const uploadPath = getTempPath();
    return fs.readdirSync(uploadPath);
};
/**
 * @returns {Array} A list of folders in the converted folder (corresponding to original file's hash)
 */
export const countUploadedChunks = (hash) => {
    const uploadPath = getTempFilePath(hash);
    return fs
        .readdirSync(uploadPath)
        .filter((elm) => elm !== "status" && elm !== "output").length;
};
/**
 * @returns {boolean} True if it was already transcoded and is present in the `transcoded` folder
 */
export const wasTranscoded = (hash) => {
    return listTranscodeFolders().some((elm) => elm === hash);
};
