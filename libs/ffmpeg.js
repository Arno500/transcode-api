import ffmpeg from "fluent-ffmpeg";

let ffmpegPath = null;
let ffprobePath = null;
export const setFfmpegPath = (path) => {
    ffmpegPath = path;
};
export const setFfprobePath = (path) => {
    ffprobePath = path;
};

export default () => {
    ffmpeg.setFfmpegPath(ffmpegPath);
    ffmpeg.setFfprobePath(ffprobePath);
    return ffmpeg;
};
