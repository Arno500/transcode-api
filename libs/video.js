import os from "os";

// import ffmpegCommand from "fluent-ffmpeg";
import ffmpegCommand from "./ffmpeg";
import { getTempOutputPath, getTranscodedOutputPath } from "../utils/paths";
import {
    retrieveTranscodeStatus,
    writeTranscodeStatus,
    copyStatus,
} from "./transcode";

const usedThreads = os.cpus().length * (process.env.THREADS_RATIO || 0.5);

export default async (job) => {
    new Promise(async (resolve, reject) => {
        const hash = job.id;
        const settings = job.data;
        const name = job.name;
        copyStatus(hash);
        const status = await retrieveTranscodeStatus(hash);
        const outputFile = getTranscodedOutputPath(hash, name, true);
        const ffmpegVideo = ffmpegCommand()(getTempOutputPath(hash), {
            priority: 5,
        });
        const filters = [];
        const outputOptions = [];

        if (settings.poster !== false) {
            const poster = getTranscodedOutputPath(hash, "poster.jpg");
            const ffmpegPoster = ffmpegCommand()(getTempOutputPath(hash), {
                priority: 5,
            });

            await (() =>
                new Promise((resolve, reject) =>
                    ffmpegPoster
                        .noAudio()
                        .withFrames(1)
                        .seekInput(settings.poster || 10)
                        .save(poster)
                        .on("end", resolve)
                        .on("error", reject)
                ))();
        }

        const fileMeta = await (() =>
            new Promise((resolve, reject) =>
                ffmpegVideo.ffprobe((err, data) => {
                    if (err) reject(err);
                    const videoStream = data.streams.find(
                        (elm) => elm.codec_type === "video"
                    );
                    const audioStream = data.streams.find(
                        (elm) => elm.codec_type === "audio"
                    );
                    resolve({ video: videoStream, audio: audioStream });
                })
            ))();

        // Output FPS
        const outputFPS = settings.fps || 30;
        const evaluatedFPS = eval(fileMeta.video.avg_frame_rate);
        ffmpegVideo.fps(evaluatedFPS < outputFPS ? evaluatedFPS : outputFPS);

        // Video codec setup
        if (settings.videoCodec === "av1") {
            ffmpegVideo.videoCodec("libsvt_av1");
            outputOptions.push("-q " + (settings.quality.video.crf || 40));
            outputOptions.push("-preset 4");
            outputOptions.push("-sc_detection true");
            outputOptions.push("-row-mt 1");
            // outputOptions.push("-g 30");
            outputOptions.push("-forced-idr");
        } else if (settings.videoCodec === "vp9") {
            ffmpegVideo.videoCodec("libvpx-vp9");
            if (settings.quality.video.crf)
                outputOptions.push("-crf " + settings.quality.video.crf);
            outputOptions.push("-row-mt 1");

            ffmpegVideo.videoBitrate(settings.quality.video.maxBitrate || 0);
        } else if (settings.videoCodec === "h264") {
            ffmpegVideo.videoCodec("libx264");
            if (settings.quality.video.crf)
                outputOptions.push("-crf " + settings.quality.video.crf);
            if (settings.quality.video.maxBitrate) {
                outputOptions.push(
                    "-maxrate " + settings.quality.video.maxBitrate
                );
                outputOptions.push(
                    "-bufsize " + settings.quality.video.maxBitrate
                );
            }
        } else {
            throw new Error(
                `Codec ${settings.videoCodec} not currently supported`
            );
        }

        // Video container setup
        if (!settings.container) {
            switch (settings.videoCodec) {
                case "av1":
                case "vp9":
                    ffmpegVideo.format("webm");
                    break;
                case "h264":
                    ffmpegVideo.format("mp4");
                    break;
            }
        } else {
            ffmpegVideo.format(settings.container);
        }

        // Audio codec setup
        if (!settings.audioCodec) {
            switch (settings.videoCodec) {
                case "av1":
                case "vp9":
                    ffmpegVideo.audioCodec("libopus").audioBitrate("128k");
                    break;
                case "h264":
                    ffmpegVideo.audioCodec("aac").audioBitrate("192k");
                    break;
                default:
                    ffmpegVideo.audioCodec("aac").audioBitrate("192k");
            }
        } else {
            switch (settings.audioCodec) {
                case "opus":
                    ffmpegVideo.audioCodec("libopus").audioBitrate("128k");
                    break;
                case "aac":
                    ffmpegVideo.audioCodec("aac").audioBitrate("192k");
                    break;
                default:
                    ffmpegVideo
                        .audioCodec(settings.audioCodec)
                        .audioBitrate(settings.quality.audioBitrate);
            }
        }

        // Video scale if asked or needed
        if (typeof settings.scale !== "undefined") {
            let width = "-1";
            let height = "-1";
            if (settings.scale.width) width = settings.scale.width;
            if (settings.scale.height) height = settings.scale.height;
            filters.push({
                filter: "scale",
                options: {
                    w: `min(${width},iw)`,
                    h: `min(${height},ih)`,
                    flags: "lanczos",
                },
            });
        }

        // Force YUV 4:2:0 for better compatibility and size
        outputOptions.push("-pix_fmt yuv420p");

        // Setup job events for progress and end
        ffmpegVideo.on("start", (commandLine) => {
            job.log(
                "Started conversion with the following command-line: " +
                    commandLine
            );
            console.log(
                "Started conversion with the following command-line: " +
                    commandLine
            );
            status.progress = 0;
            writeTranscodeStatus(hash, status);
        });
        ffmpegVideo.on("codecData", (data) => {
            job.log("The input file has the following caracteristics: ");
            job.log(data);
        });
        ffmpegVideo.on("error", (err, stdout, stderr) => {
            job.log("An error occured during conversion");
            console.error("An error occurred during conversion", err);
            status.status = "failed";
            delete status.progress;
            writeTranscodeStatus(hash, status);
            job.moveToFailed(err);
            reject(err);
        });
        ffmpegVideo.on("progress", (data) => {
            job.updateProgress(Math.trunc(data.percent * 100) / 100);
            status.progress = data.percent;
            writeTranscodeStatus(hash, status);
        });
        ffmpegVideo.on("end", (stdout, stderr) => {
            console.log(`Conversion of ${hash} finished successfully`);
            status.status = "completed";
            delete status.progress;
            writeTranscodeStatus(hash, status);
            job.moveToCompleted();
            resolve();
        });

        // End setup and start processing
        ffmpegVideo.videoFilters(filters);
        outputOptions.push("-threads " + usedThreads);
        outputOptions.push("-map_metadata -1");
        ffmpegVideo.outputOptions(outputOptions);
        ffmpegVideo.save(outputFile);
    });
};

/* AV1
Latest generation codec. Not decoded by hardware for now, future-proof. Don't use now, it's really inneficient (we're talking about 5% CPU usage on a Ryzen 3700X, and 0.1 FPS for a FHD encode). I'm searching a way to offer a Linux build with SVT-AV1, which is MUCH faster (3-4 FPS depending on settings).

* -c:v libaom-av1
For row based multithreading: * -row-mt 1
For CRF:
    * -crf 25
    * -b:v 0 (NEEDED)
For CQ:
    * -crf 25
    * -b:v 2000k

* -strict experimental
Default to Opus for sound as every browser supporting AV1 also supports Opus
*/

/* VP9
Alternative to HEVC, more suited to web + no royalties for browsers implementing it.

* -c:v libvpx-vp9
For row based multithreading: * -row-mt 1

For CRF:
    * -crf 25
    * -b:v 0 (NEEDED)
For CQ:
    * -crf 25
    * -b:v 2000k

Default to Opus for sound as every browser supporting VP9 also supports Opus
*/

/* H264
Classic old-fashioned codec. Works after IE9 and almost every old browser (which no one should be using by the way)

* -c:v libx264
For CRF:
    * -crf 22
For CQ:
    * -crf 22
    * -maxrate 1M
    * -bufsize (would say equal to the max rate)
    
    Default to AAC for sound as almost every browser supporting H264 also supports AAC
    */

/* MP4
    * -movflags +faststart

*/

/* AAC
    * -c:a aac
    * -b:a 192k

*/

/* Opus
    * -c:a libopus
    * -b:a 128k

*/

/* General settings
    * -threads (number of detected CPU threads / 2)
   Output:
    * -r (calculate framerate and ceil at 30 if it's >. Also check if it wasn't specified in settings)
    * -vf "scale='min(settings.width,iw)':'min(settings.height,ih)':flags=lanczos" (specify -1 if not specified in the input)
    * -pix_fmt yuv420p
   Poster:
    * -vframes 1 -an -ss poster.jpg (10 is the number of seconds where to take the still)
*/
