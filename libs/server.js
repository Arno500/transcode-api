import fs from "fs";

import fastify from "fastify";
import fastifyMultipart from "fastify-multipart";
import fastifyExpress from "fastify-express";
import fastifyStatic from "fastify-static";
import { setQueues, router as bullBoardUI } from "bull-board";

import { videoQueue, uploadCleaningQueue } from "./queues";
import {
    getTempFilePath,
    getTempChunkPath,
    getTranscodedPath,
} from "../utils/paths";
import {
    assembleChunks,
    createNewTranscode,
    retrieveTranscodeStatus,
    writeTranscodeStatus,
    wasTranscoded,
    countUploadedChunks,
} from "./transcode";
import { currentPlatform } from "../utils/platform";

// Fastify init
const server = fastify({
    logger: true,
});
server.register(fastifyMultipart, { limits: { files: 1 } });
server.register(fastifyStatic, { root: getTranscodedPath(), serve: false });

// Default route if someone goes on that page
server.get("/", async (request, reply) => {
    return "This is an API. Please contact the server owner to get the documentation and the tokens to use it.";
});

// Bull-board setup
(async () => {
    await server.register(fastifyExpress);
    setQueues([videoQueue, uploadCleaningQueue]);
    server.use("/queues", bullBoardUI);
})();

// Create new transcode
const containersEnum = ["vp9", "h264"];
server.post(
    "/new",
    {
        schema: {
            body: {
                type: "object",
                required: ["filename", "hash", "chunks", "size", "settings"],
                properties: {
                    filename: {
                        type: "string",
                    },
                    hash: {
                        type: "string",
                    },
                    chunks: {
                        type: "integer",
                        minimum: 1,
                    },
                    size: {
                        type: "integer",
                    },
                    settings: {
                        type: "object",
                        required: ["videoCodec", "quality"],
                        properties: {
                            videoCodec: {
                                type: "string",
                                enum: ["av1", "vp9", "h264"],
                            },
                            audioCodec: {
                                type: "string",
                                enum: ["aac", "opus"],
                            },
                            container: {
                                type: "string",
                                enum:
                                    currentPlatform === "win32"
                                        ? [...containersEnum, "av1"]
                                        : containersEnum,
                            },
                            poster: {
                                type: ["string", "integer"],
                            },
                            fps: {
                                type: "integer",
                            },
                            quality: {
                                type: "object",
                                properties: {
                                    video: {
                                        type: "object",
                                        required: ["crf"],
                                        properties: {
                                            crf: {
                                                type: "integer",
                                            },
                                            maxBitrate: {
                                                type: ["integer", "string"],
                                            },
                                        },
                                    },
                                    audio: {
                                        type: "object",
                                        required: ["bitrate"],
                                        properties: {
                                            bitrate: {
                                                type: ["integer", "string"],
                                            },
                                        },
                                    },
                                },
                            },
                            scale: {
                                type: "object",
                                properties: {
                                    height: {
                                        type: "integer",
                                    },
                                    width: {
                                        type: "integer",
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
    async (req, reply) => {
        if (
            typeof (await uploadCleaningQueue.getJob(req.body.hash)) !==
            "undefined"
        ) {
            reply.code(409).send({
                error: {
                    status: "already_declared",
                    message:
                        "The file has already been declared. Please continue uploading the parts, or wait 72h for the file to remove itself. You can also query its status and upload the missing parts.",
                },
            });
            return;
        }
        if (wasTranscoded(req.body.hash)) {
            reply.code(409).send({
                error: {
                    status: "previously_converted",
                    message:
                        "The file has previously already been converted. You probably should reuse that file, to save earth ressources and compute time for better uses like Folding@Home.",
                },
            });
        }
        const result = await createNewTranscode(req.body);
        reply.code(200).send(result);
        return;
    }
);

server.put("/:hash/part/:id", async (req, reply) => {
    if (!req.isMultipart()) {
        reply
            .code(400)
            .send(
                new Error(
                    "Request is not multipart. How did you expect to send file data?"
                )
            );
        return;
    }
    const hash = req.params.hash;
    const tempPath = getTempFilePath(hash);
    if (tempPath === false) {
        reply.code(404).send();
        return;
    }
    const handler = async (field, file) => {
        const chunkPath = getTempChunkPath(hash, req.params.id);
        try {
            const result = await (() =>
                new Promise((resolve, reject) =>
                    file
                        .pipe(fs.createWriteStream(chunkPath))
                        .on("close", async (err) => {
                            if (err) reject(err);
                            const status = await retrieveTranscodeStatus(hash);
                            status.chunksStatus[req.params.id] = fs.statSync(
                                chunkPath
                            ).size;
                            const returnData = await writeTranscodeStatus(
                                hash,
                                status
                            );
                            return resolve(returnData);
                        })
                ))();
            reply.code(200).send(result);
        } catch (err) {
            reply.code(500).send();
        }
    };
    const mp = req.multipart(handler, (err) => {
        if (err) reply.code(500).send(err);
    });
});

server.post("/:hash/finalize", async (req, reply) => {
    const hash = req.params.hash;
    const tempPath = getTempFilePath(hash);
    if (tempPath === false) {
        reply.code(404).send();
        return;
    }
    const status = await retrieveTranscodeStatus(hash);
    if (status.chunksStatus.length !== countUploadedChunks(hash)) {
        reply.code(400).send({
            error: {
                status: "bad_chunk_count",
                message:
                    "Some chunks are missing. The missing chunks are marked as false in the data key of the error.",
                data: status,
            },
        });
        return;
    }
    try {
        await assembleChunks(hash, status.chunksStatus.length);
        status.status = "uploaded";
        writeTranscodeStatus(hash, status);
        const job = await videoQueue.add(status.filename, status.settings, {
            jobId: hash,
            attempts: 3,
        });
        reply.code(204).send();
        return;
    } catch {
        reply.code(449).send({
            error: {
                status: "bad_data",
                message:
                    "The resulting file does not have the same MD5 hash as transmitted by the client. Maybe try to compare the received chunk sizes in the data key of the error.",
            },
            data: status,
        });
        return;
    }
});

server.get("/:hash/status", async (req, reply) => {
    try {
        const result = await retrieveTranscodeStatus(req.params.hash, true);
        reply.code(200).send(result);
    } catch (err) {
        reply.code(404).send();
    }
});

server.get("/:hash/:file", async (req, reply) => {
    const badPath = /\.\./;
    if (
        badPath.match(req.params.hash) ||
        badPath.match(req.params.file) ||
        req.params.file === "status"
    )
        reply
            .code(403)
            .send(
                "You probably tried to access something you did not have the right to. Evil person, bad, bad, crackhead, nightmare, nightmare, nightmare..."
            );
    reply.sendFile(getTranscodedFilePath(req.params.hash, req.params.file));
});

export const startServer = () => {
    server.listen(3000, function (err, address) {
        if (err) {
            server.log.error(err);
            process.exit(1);
        }
        server.log.info(`server listening on ${address}`);
    });
};
